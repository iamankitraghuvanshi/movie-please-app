## Movie Please Application - Code Challenge

This is a movie please application powered by Sogeti. Users can see featured movies on the homepage and search movies by title of the movie. Further, users can choose `Full` or `Short` Plot description while searhing movies.
This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.0.3.

### Author 
- Ankit Raghuvanshi

### Local Development
1. Install - `ng serve`
2. Localhost URL - `http://localhost:4200/`
3. Run Unit tests- `ng test`
4. Run End-to-End tests - `ng e2e`
5. Run automated documentation - `npm run compodoc` under `documentation/index.html` directory
6. Production Package - `ng build --prod` under `dist/` directory

## Highlights of the project - completed Bonus Requirements

### 1. When searching movies plot can be set to either ‘full‘ or ‘short’.
- User can search with full/ short plot values

### 2. Read more functionality for full plot after 200 characters.
- By default truncated values of plot (200 chars) will be shown with `show more` and `show less` funcitonality

### 3. Display genre, writer and actors as a list instead of comma separated.
- User can see Genre, Writer, Actors in a list form

### 4. Responsive design
- User can also use this app on handheld devices

### 5. Automated Documentation
- 100% automated documentation of the project

### Glimpse
![MP](https://bitbucket.org/iamankitraghuvanshi/movie-please-app/raw/master/iphoneX.png)