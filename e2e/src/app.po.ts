import { browser, by, element } from 'protractor';

export class AppPage {
  navigateTo() {
    return browser.get(browser.baseUrl) as Promise<any>;
  }

  getTitle() {
    return element(by.css('app-root h1 a img[alt="Movie Please"]'));
  }

  getFeaturedMovies() {
    return element.all(by.css('.mp-featured-movies .mp-movie-tile'));
  }

  getSearchBar() {
    return element(by.css('.mp-search-bar'));
  }

  getSearchInput() {
    return element(by.id('mp-search-input'));
  }

  getSearchResults() {
    return element.all(by.css('.mp-search-results .mp-movie-tile'));
  }

  getBackToHome() {
    return element.all(by.css('.mp-back-nav'));
  }
}
