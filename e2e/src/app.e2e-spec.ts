import { AppPage } from './app.po';
import { browser, logging, by, protractor } from 'protractor';

describe('workspace-project App', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });

  it('should check if movie please logo is present', () => {
    page.navigateTo();
    expect(page.getTitle().isPresent()).toBe(true)
  });

  it('should display two featured movie', () => {
    page.navigateTo();
    expect(page.getFeaturedMovies().count()).toBe(2);
  });

  it('should display two featured movie with details', () => {
    page.navigateTo();
    expect(page.getFeaturedMovies().count()).toBe(2);
    expect(page.getFeaturedMovies().get(0).all(by.css('figure img')).isDisplayed()).toBeTruthy();
    expect(page.getFeaturedMovies().get(0).all(by.css('mp-fm-details h3')).isDisplayed()).toBeTruthy(); 
    expect(page.getFeaturedMovies().get(0).all(by.css('mp-fm-details p')).isDisplayed()).toBeTruthy(); 
    expect(page.getFeaturedMovies().get(0).all(by.css('mp-fm-details .mp-booknow')).isDisplayed()).toBeTruthy(); 
  });

  it('should display search bar on the page', () => {
    page.navigateTo();
    expect(page.getSearchBar().isPresent()).toBe(true);
  });

  it('should check search bar elements with value', () => {
    page.navigateTo();
    expect(page.getSearchBar().all(by.css('.mp-searchInput input')).isDisplayed()).toBeTruthy();
    expect(page.getSearchBar().all(by.css('.mp-select-plot fieldset')).count()).toBe(2);
    expect(page.getSearchBar().all(by.css('.mp-select-plot fieldset')).get(0).all(by.css('input')).getAttribute('checked')).toEqual(['true'])
  });

  it('should open search page, when user enters any movie title `Bridge` in the search input', () => {
    page.navigateTo();
    page.getSearchInput().click();
    page.getSearchInput().sendKeys('Bridge', protractor.Key.ENTER);
    
    expect(browser.getCurrentUrl()).toContain("/search?searchQuery=Bridge&plotSelect=full");
  });

  it('should display top 5 search results for movie title starting with `Bridge`', () => {
    page.navigateTo();
    page.getSearchInput().click();
    page.getSearchInput().sendKeys('Bridge', protractor.Key.ENTER);
    
    expect(browser.getCurrentUrl()).toContain("/search?searchQuery=Bridge&plotSelect=full");
    expect(page.getSearchResults().count()).toBe(5);
  });

  it('should display 5 search results with details of movie starting with `Bridge`', () => {
    page.navigateTo();
    page.getSearchInput().click();
    page.getSearchInput().sendKeys('Bridge', protractor.Key.ENTER);

    expect(browser.getCurrentUrl()).toContain("/search?searchQuery=Bridge&plotSelect=full");
    expect(page.getSearchResults().count()).toBe(5);
    expect(page.getSearchResults().get(0).all(by.css('figure img')).isDisplayed()).toBeTruthy();
    expect(page.getSearchResults().get(0).all(by.css('mp-fm-details h3')).isDisplayed()).toBeTruthy(); 
    expect(page.getSearchResults().get(0).all(by.css('.movie-info')).count()).toBe(10);
    expect(page.getSearchResults().get(0).all(by.css('mp-fm-details .mp-booknow')).isDisplayed()).toBeTruthy(); 
  });

  it('should load homepage when user clicks on `Back to Homepage` link', () => {
    page.navigateTo();
    page.getBackToHome().click();

    expect(browser.getCurrentUrl()).toEqual('http://localhost:4200/');
  });

  it('should check if Actor, Writer, Genre are displayed in a list', () => {
    page.navigateTo();
    page.getSearchInput().click();
    page.getSearchInput().sendKeys('Bridge', protractor.Key.ENTER);

    expect(browser.getCurrentUrl()).toContain("/search?searchQuery=Bridge&plotSelect=full");
    expect(page.getSearchResults().get(0).all(by.css('.mp-item-list .mp-genre')).isDisplayed()).toBeTruthy();
    expect(page.getSearchResults().get(0).all(by.css('.mp-item-list .mp-actors')).isDisplayed()).toBeTruthy();
    expect(page.getSearchResults().get(0).all(by.css('.mp-item-list .mp-writer')).isDisplayed()).toBeTruthy();
    
  });


  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER);
    expect(logs).not.toContain(jasmine.objectContaining({
      level: logging.Level.SEVERE,
    } as logging.Entry));
  });
});
