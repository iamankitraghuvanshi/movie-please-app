import { TestBed, async, ComponentFixture, inject } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { Router } from '@angular/router';
import { RoutingComponents } from './app-routing.module';
import { RouterTestingModule } from '@angular/router/testing'
import { Component } from '@angular/core';

describe('AppComponent', () => {
  let component: testHostComponent;
  let fixture: ComponentFixture<testHostComponent>;
  let router: Router;
  let mockRouter;
  beforeEach(async(() => {
    
    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        RoutingComponents,
        testHostComponent
      ],
      imports: [ RouterTestingModule.withRoutes([]) ],
      providers: [{
        provide: Router,
        useValue: {
          navigate: jasmine.createSpy('navigate')
        }
      }]
      
    }).compileComponents();
  }));
  
  beforeEach(() => {
    fixture = TestBed.createComponent(testHostComponent);
    router = TestBed.get(Router)
    component = fixture.componentInstance;
    router = jasmine.createSpyObj('router', ['navigate']);
    fixture.detectChanges();
  });

  describe('handleSearch method testing', () => {
    it('should navigate to search page with title', inject([ Router ], (router) => {
      const comp = new AppComponent(router);
      const value = {
          title: 'Bridge'
      };

      comp.handleSearch(value)
      
      expect(router.navigate).toHaveBeenCalledWith(['./search'], { queryParams : value});
    }));
    
    it('should navigate to search page with title and plot', inject([ Router ], (router) => {
      const comp = new AppComponent(router);
      const value = {
          title: 'Bridge',
          plot: 'full'
      };

      comp.handleSearch(value)
      
      expect(router.navigate).toHaveBeenCalledWith(['./search'], { queryParams : value});
    }));

  })
  
  
  @Component({
    selector: `host-component`,
    template: `<app-root></app-root>`
  })
  class testHostComponent {
   
  }
});
