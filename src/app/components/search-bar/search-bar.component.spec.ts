import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchBarComponent } from './search-bar.component';
import { Component } from '@angular/core';

describe('SearchBarComponent', () => {
  let component: testHostComponent;
  let fixture: ComponentFixture<testHostComponent>;
  
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchBarComponent, testHostComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(testHostComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  describe('searchMovies method testing', () => {
    it('should emit events for search query = `Bridge` and plot = `full`', () => {
      const comp = new SearchBarComponent();
      comp.searchInput = {
        nativeElement: {
          value: 'Bridge'
        }
      };
      comp.plotShortSelect = {
        nativeElement: {
          checked: true,
          value: 'full'
        }
      };
      
      const value = { searchQuery: 'Bridge', plotSelect: 'full' };
      
      spyOn(comp.searchEvent, 'emit' );
      comp.searchMovies({ keyCode: 13});

      expect(comp.searchEvent.emit).toHaveBeenCalledWith(value);
    });  

    it('should emit events for search query = `Bridge` and plot = `short`', () => {
      const comp = new SearchBarComponent();
      comp.searchInput = {
        nativeElement: {
          value: 'Bridge'
        }
      };
      comp.plotShortSelect = {
        nativeElement: {
          checked: false,
          value: 'short'
        }
      };
      
      const value = { searchQuery: 'Bridge', plotSelect: 'full' };
      
      spyOn(comp.searchEvent, 'emit' );
      comp.searchMovies({ keyCode: 13});

      expect(comp.searchEvent.emit).toHaveBeenCalledWith(value);
    });  
  });
  
  @Component({
    selector: `host-component`,
    template: `<search-bar></search-bar>`
  })
  class testHostComponent {
   
  }
  
});
