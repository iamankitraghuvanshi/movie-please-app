import { Component, ElementRef, EventEmitter, Output, ViewChild, ViewEncapsulation } from '@angular/core';

/**
 * Search Bar component
 */
@Component({
  selector: 'search-bar',
  templateUrl: './search-bar.component.html',
  styleUrls: ['./search-bar.component.scss'],
  encapsulation: ViewEncapsulation.None
})

export class SearchBarComponent {
  /** 
   * @ignore
  */
  public movieTitle: String = "Bridge";
  /** 
   * @ignore
  */
  movie: { plot: Array<any>; };

  /**
   * Search Input Child
   */
  @ViewChild('searchInput', { static: true })
  searchInput: ElementRef;
  
  /**
   * Plot Short Select 
   */
  @ViewChild('plotShortSelect', { static: true })
  plotShortSelect: ElementRef

  /**
   * Output for EventEmitter
   */
  @Output()
  searchEvent = new EventEmitter<Object>();
  plotShortEvent = new EventEmitter<Object>();

  /**
  * The "constructor"
  */
  constructor() { 
    this.movie = {
      plot: ['full', 'short']
    }
  }
  
  /**
  * This method is used to search movies
  * @param {Object} event  element event
  * 
  * @example
  * searchMovies()
  */
  searchMovies(event) {
    if (event.keyCode === 13) {
      const searchTerm = this.searchInput.nativeElement.value;
      const plotSelect = this.plotShortSelect.nativeElement.checked ? this.plotShortSelect.nativeElement.value : 'full';
      
      this.searchEvent.emit({ searchQuery: searchTerm, plotSelect: plotSelect});
    }
  }

}
