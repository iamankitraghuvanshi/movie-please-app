import { async, ComponentFixture, TestBed, inject } from '@angular/core/testing';
import { SearchPageComponent } from './search-page.component';
import { MoviePleaseService } from 'src/app/data-access/movie-please.service';
import { RouterTestingModule } from '@angular/router/testing';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { Component } from '@angular/core';


describe('SearchPageComponent', () => {
  let component: testHostComponent;
  let fixture: ComponentFixture<testHostComponent>;
  let router: Router;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchPageComponent, testHostComponent ],
      imports: [ HttpClientModule, RouterTestingModule.withRoutes([]) ],
      providers: [ MoviePleaseService, 
        {
          provide: ActivatedRoute,
          useValue: {
            snapshot: {
              queryParams: {
                searchQuery: 'Bridge',
                plotSelect: 'full'
              }
            }
          }
        },
        {
          provide: Router,
          useValue: {
            events: jasmine.createSpyObj('events', ['subscribe'])
          }
        }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchPageComponent);
    router = TestBed.get(Router);
    component = fixture.componentInstance;
    router = jasmine.createSpyObj('router', ['events']);
    fixture.detectChanges();
    // spyOn(router.events, 'subscribe').and.callThrough();
  });

  describe('ngOnInit method testing', () => {
    it('should check initialization', inject([ MoviePleaseService, ActivatedRoute, Router ], (moviePlease, route, router) => {
      const comp = new SearchPageComponent(moviePlease, route, router);
      spyOn(comp, 'searchMovies');
      comp.ngOnInit();

      expect(comp.searchMovies).toHaveBeenCalled();
      expect(router.events.subscribe).toHaveBeenCalled();
    }));
  });
  
  describe('processMovieObj method testing', () => {
    it('should process each movie object for BONUS REQUIREMENTS', inject([ MoviePleaseService, ActivatedRoute, Router ], (moviePlease, route, router) => {
      const comp = new SearchPageComponent(moviePlease, route, router);
      const input = {
        Actors: 'a, b, c',
        Writer: 'x, y, z',
        Genre: 'aa, bb, cc',
        Plot: `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum`
      };

      comp.processMovieObj(input);

      expect(input).toBe(input)
    }));
  });

  describe('showMoreLess method testing', () => {
    it('should check `SHOW MORE` functionality for BONUS REQUIREMENTS', inject([ MoviePleaseService, ActivatedRoute, Router ], (moviePlease, route, router) => {
      const comp = new SearchPageComponent(moviePlease, route, router);
      const input = {
        Actors: 'a, b, c',
        Writer: 'x, y, z',
        Genre: 'aa, bb, cc',
        Plot: `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum`
      };

      const event = {
        target: { 
          textContent: 'Show More'
        }
      };
      
      comp.showMoreLess(input, event);

      expect(event.target.textContent).toBe('Show Less');
    }));

    it('should check `SHOW LESS` functionality for BONUS REQUIREMENTS', inject([ MoviePleaseService, ActivatedRoute, Router ], (moviePlease, route, router) => {
      const comp = new SearchPageComponent(moviePlease, route, router);
      const input = {
        Actors: 'a, b, c',
        Writer: 'x, y, z',
        Genre: 'aa, bb, cc',
        Plot: `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum`
      };

      const event = {
        target: { 
          textContent: 'Show Less'
        }
      };
      
      comp.showMoreLess(input, event);

      expect(event.target.textContent).toBe('Show More');
    }));

  });

  @Component({
    selector: `host-component`,
    template: `<search-page></search-page>`
  })
  class testHostComponent {
   
  }
});
