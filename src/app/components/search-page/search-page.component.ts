import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { MoviePleaseService } from './../../data-access/movie-please.service';
import { ActivatedRoute, Router, ActivationEnd } from "@angular/router";

/**
 * Search Page component
 */
@Component({
  selector: 'search-page',
  templateUrl: './search-page.component.html',
  styleUrls: ['./search-page.component.scss'],
  providers: [ MoviePleaseService ],
  encapsulation: ViewEncapsulation.None
})

/**
 * Class SearchPageComponent with 
 * method handleSearch(), ngOnInit(), processMOvieObj(), showMoreLess()
 */
export class SearchPageComponent implements OnInit {
  /** 
   * @ignore
  */
  public searchedMovies: Object[] = [];
  /** 
   * @ignore
  */
  private topMovies = 5;
  /** 
   * @ignore
  */
  private pageNotFound: boolean = false;
  /** 
   * @ignore
  */
  private allImDBIds = [];
  /** 
   * @ignore
  */
  urlParam: any;

  /**
  * The "constructor"
  *
  * @param { MoviePleaseService } moviePlease A moviePlease service
  * @param { ActivatedRoute } route A ActivatedRoute service
  * @param { Router } router A Router service
  */
  constructor(private moviePlease: MoviePleaseService, private route: ActivatedRoute, private router: Router) {
    this.moviePlease = moviePlease;
    this.route = route;
    this.router = router;
  }

  /**
  * This method is used to initialize
  * and call searchMovies() method
  * @example
  * ngOnInit()
  */
  ngOnInit() {    
    this.searchMovies();  
    /* This is to check of changed search query*/
    this.router.events.subscribe(e => {
      if(e instanceof ActivationEnd) {
        this.urlParam = e.snapshot.queryParams;
        this.searchMovies();
      }
    });
  }

  /**
  * This method is used to search movies
  * 
  * @example
  * searchMovies()
  */
  searchMovies() {
    this.searchedMovies = [];
    this.urlParam = this.route.snapshot.queryParams;
    
    this.moviePlease.searchMovies(this.urlParam).subscribe(data => {
      if (data['Response'] !== 'False') {
        this.pageNotFound = false;
        this.allImDBIds = data['Search'].slice(0, this.topMovies).map(( m ) => {
          const { imdbID } = m;
          return imdbID;
        });

        this.allImDBIds.forEach(id => {
          this.moviePlease.featuredMovies(id, this.urlParam.plotSelect).subscribe(movie => {
            this.searchedMovies.push(this.processMovieObj(movie))
          });
        });

      } else {
        this.searchedMovies = [];
        this.pageNotFound = true;
      } 
    });
    
  }

  /**
  * This method is used to process movie object
  * @param {Object} movie individual movie object
  * 
  * @example
  * processMovieObj()
  */
  processMovieObj(movie) {
    movie.Actors = movie.Actors.split(', ');
    movie.Writer = movie.Writer.split(', ');
    movie.Genre = movie.Genre.split(', ');
    movie.shortPlot = movie.Plot.substring(0, 200) + '...';
    
    return movie;
  }

  /**
  * This method is used for show more/ less 
  * functionality
  * @param {Object} movie individual movie object
  * @param {Object} event element event object
  * 
  * @example
  * showMoreLess()
  */
  showMoreLess(movie, event) {
    if(event.target.textContent === 'Show More') {
      movie.shortPlot = movie.Plot;
      event.target.textContent = 'Show Less'
    } else {
      movie.shortPlot = movie.Plot.substring(0, 200) + '...';
      event.target.textContent = 'Show More'
    }
    
  }
}
