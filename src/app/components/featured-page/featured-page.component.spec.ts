import { async, ComponentFixture, TestBed, inject } from '@angular/core/testing';

import { FeaturedPageComponent } from './featured-page.component';
import { MoviePleaseService } from 'src/app/data-access/movie-please.service';
import { HttpClientModule } from '@angular/common/http';

describe('FeaturedPageComponent', () => {
  let component: FeaturedPageComponent;
  let fixture: ComponentFixture<FeaturedPageComponent>;
  let moviePlease: MoviePleaseService;
  
  let moviePleaseSpy;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FeaturedPageComponent ],
      imports: [ HttpClientModule ],
      providers: [ MoviePleaseService ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FeaturedPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    moviePlease = fixture.debugElement.injector.get(MoviePleaseService);
    spyOn(component, 'getFeaturedMovies').and.returnValue();
  });

  describe('ngOnInit method testing', () => {
    it('should call getFeaturedMovies method', () => {
      component.ngOnInit();
      expect(component.getFeaturedMovies).toHaveBeenCalled();
    });  
  });

  describe('getFeaturedMovies method testing', () => {
    it('should call featuredMovies service and get two featured movies', async( 
      inject( [MoviePleaseService], ( moviePlease ) => {
        component.getFeaturedMovies();
        fixture.whenStable().then( () => {
            expect( component.featuredMovies ).toBeDefined();
            expect( component.featuredMovies.length ).toEqual( 2 );
            moviePlease.featuredMovies();
            return fixture.whenStable();
        })  
      } ) 
    ));
  });
});
