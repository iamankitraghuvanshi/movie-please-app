import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { MoviePleaseService} from './../../data-access/movie-please.service';
/**
 * Featured Page component
 */
@Component({
  selector: 'featured-page',
  templateUrl: './featured-page.component.html',
  styleUrls: ['./featured-page.component.scss'],
  providers: [ MoviePleaseService ],
  encapsulation: ViewEncapsulation.None
})

export class FeaturedPageComponent implements OnInit {
  /** 
   * @ignore
  */
  public featuredMovies: Object[] = [];
  /** 
   * @ignore
  */
  pageNotFound: boolean = false;
  /** 
   * @ignore
  */
  imdbIDs: string[];
  /** 
   * @ignore
  */
  plot: string;

  /**
  * The "constructor"
  *
  * @param {MoviePleaseService} moviePlease  A moviePlease data service
  */
  constructor( private moviePlease: MoviePleaseService) {
    this.moviePlease = moviePlease;
  }

  /**
  * This method is used to load the method getFeaturedMovies
  * for displaying default two movies with details.
  * @example
  * ngOnInit()
  */
  ngOnInit() {
    this.getFeaturedMovies();
  }

  /**
  * This method is used to shpw default two movies with details.
  * @example
  * getFeaturedMovies()
  */
  getFeaturedMovies() {
    this.imdbIDs = ['tt1872181', 'tt0398808'];
    this.plot = 'full';

    this.imdbIDs.forEach((imdbID: String) => {
      this.moviePlease.featuredMovies(imdbID, this.plot).subscribe((movies) => {
        if (movies['Response'] !== 'False') {
          this.pageNotFound = false;
          this.featuredMovies.push(movies)
        } else {
          this.featuredMovies = [];
          this.pageNotFound = true;
        }
      });  
    });
  }

}
