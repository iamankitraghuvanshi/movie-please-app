import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { SearchPageComponent } from './components/search-page/search-page.component';
import { FeaturedPageComponent } from './components/featured-page/featured-page.component';
import { SearchBarComponent } from './components/search-bar/search-bar.component';

/**
* Routes for the application
*/
export const routes: Routes = [
  {
    path: '', 
    component: FeaturedPageComponent
  }, {
    path: 'search', 
    component: SearchPageComponent
  }
];

/**
* App routing module
*/
@NgModule({
  exports: [ RouterModule ],
  imports: [
    RouterModule.forRoot(routes)
  ]
})
export class AppRoutingModule { }
/**
* Routing Components configurations
*/
export const RoutingComponents = [
  AppComponent,
  SearchPageComponent,
  FeaturedPageComponent,
  SearchBarComponent
];