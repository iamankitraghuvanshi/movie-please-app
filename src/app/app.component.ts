import { Component } from '@angular/core';
import { Router } from '@angular/router';

/**
 * App component
 */
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

/**
 * Class App Component with 
 * method handleSearch()
 */
export class AppComponent {
  /**
  * The "constructor"
  *
  * @param {Router} router A router service
  */
  constructor(private router: Router) { 
    this.router = router;
  }

  /**
  * This method is used to handle search events
  * @param {Object} event event
  * 
  * @example
  * handleSearch()
  */
  handleSearch(event) {
    this.router.navigate(['./search'], { queryParams: event });
  }
}
