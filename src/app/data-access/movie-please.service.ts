import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { FeaturedMovie } from './interfaces/featured-movie';
import { SearchMovie } from './interfaces/search-movie';

/** 
* @ignore
*/
@Injectable()
export class MoviePleaseService {
  /** 
  * @ignore
  */
  private apiKey = `6c3a2d45`;
  
  /**
  * The "constructor"
  *
  * @param {HttpClient} http An Http Client service
  */  
  constructor(private http: HttpClient) {
    this.http = http;
  }

  /**
  * This method is used to get list of all the
  * searched movies
  * @param {Object} query this object contains url parameters like searchQuery ex; Bridge, plotSelect ex: full/ short
  *
  * @example
  * searchMovies()
  */
  searchMovies(query): Observable<SearchMovie[]> {
    const url = `http://www.omdbapi.com/?s=${query.searchQuery}&plot=${query.plotSelect}&apiKey=${this.apiKey}`;
    return this.http.get<SearchMovie[]>(url)
                .pipe( map(res => { return res} ) );
  }

  /**
  * This method is used to get list of all the
  * featured movies
  * @param {String} imdbID this parameter is ibDBID of a movie
  * @param {String} plot this parameter contains full/ short value for plot
  *
  * @example
  * featuredMovies()
  */
  featuredMovies(imdbID: String, plot: String): Observable<FeaturedMovie[]> {
    const url = `http://www.omdbapi.com/?i=${imdbID}&plot=${plot}&apiKey=${this.apiKey}`;
    return this.http.get<FeaturedMovie[]>(url)
                .pipe( map(res => { return res }) );
  }
}
