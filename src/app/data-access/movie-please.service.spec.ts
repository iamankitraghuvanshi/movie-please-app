import { TestBed } from '@angular/core/testing';

import { MoviePleaseService } from './movie-please.service';
import { FeaturedPageComponent } from '../components/featured-page/featured-page.component';
import { SearchPageComponent } from '../components/search-page/search-page.component';

describe('MoviePleaseService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    declarations: [ SearchPageComponent, FeaturedPageComponent ],
  }));

  xit('should be created', () => {
    const service: MoviePleaseService = TestBed.get(MoviePleaseService);
    expect(service).toBeTruthy();
  });
});
