/**
* This is Search Movie interface 
*/
export interface SearchMovie {
    /** 
    * It conatins poster value
    */
    poster: String,
    /** 
    * It conatins title value
    */
    title: String,
    /** 
    * It conatins year value
    */
    year: Date,
    /** 
    * It conatins type value
    */
    type: String,
    /** 
    * It conatins rated value
    */
    Rated: String,
    /** 
    * It conatins genre value
    */
    Genre: String,
    /** 
    * It conatins director value
    */
    Director: String,
    /** 
    * It conatins actors value
    */
    Actors: String,
    /** 
    * It conatins plot value
    */
    Plot: String,
    /** 
    * It conatins awards value
    */
    Awards: String

}
