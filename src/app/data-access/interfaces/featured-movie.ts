/**
* This is Featured Movie interface 
*/
export interface FeaturedMovie {
    /** 
    * It conatins poster value
    */
    poster: String,
    /** 
    * It conatins title value
    */
    title: String,
    /** 
    * It conatins year value
    */
    year: Date,
    /** 
    * It conatins plot value
    */
    Plot: String,
    /** 
    * It conatins awards value
    */
    Awards: String

}
